var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var PomeloDemo = (function () {
    function PomeloDemo() {
        /** 初始化一个 Pinus对象 其实也就是 Pomelo对象 的TS版本 */
        this.pinus = new Pinus();
        /** gate服务器 的地址 */
        this.gateServer = {
            host: "127.0.0.1",
            port: 4000
        };
        /** gate服务器 查询 有空的 connector服务器 的路由 */
        this.gateRouteQueryEnter = "gate.gateHandler.entry";
        /** 用户id */
        this.uid = "nero";
        /** 用户所在的房间 */
        this.rid = "neroRoom";
    }
    /** 运行demo */
    PomeloDemo.prototype.run = function () {
        return __awaiter(this, void 0, void 0, function () {
            var conAdress, conres;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.accessGateServer()];
                    case 1:
                        conAdress = _a.sent();
                        console.log("拿到的connector服务器地址：", conAdress);
                        if (conAdress.code != 200) {
                            console.log("服务器请求失败");
                            return [2 /*return*/];
                        }
                        //监听channel的消息
                        this.listenChannel();
                        return [4 /*yield*/, this.accessConnector(conAdress, "connector.entryHandler.entry")];
                    case 2:
                        conres = _a.sent();
                        console.log("connector服务器返回的channel所有加入用户：", conres);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 访问gate服务器 */
    PomeloDemo.prototype.accessGateServer = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.pinus.init({
                host: _this.gateServer.host,
                port: _this.gateServer.port
                // log: true
            }, function (res) {
                // console.log("this.pinus.init.res: ", res);
                _this.pinus.request(_this.gateRouteQueryEnter, {
                    uid: _this.uid
                }, function (queryRes) {
                    // console.log("空余的connector服务器：", queryRes);
                    _this.pinus.disconnect();
                    return resolve(queryRes);
                });
            });
        });
    };
    /** 访问connector服务器
     * @param conAdress connector服务器地址
     * @param router 要访问的路由
    */
    PomeloDemo.prototype.accessConnector = function (conAdress, router) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.pinus.init({
                host: conAdress.host,
                port: conAdress.port
                // log: true
            }, function (res) {
                // console.log("this.pinus.init.res: ", res);
                _this.pinus.request(router, {
                    rid: _this.rid,
                    username: _this.uid
                }, function (queryRes) {
                    // console.log("空余的connector服务器：", queryRes);
                    return resolve(queryRes);
                });
            });
        });
    };
    /** 监听服务器的channel消息 */
    PomeloDemo.prototype.listenChannel = function () {
        this.pinus.on("onAdd", function (data) {
            console.log("监听到的消息：", data);
        });
    };
    return PomeloDemo;
}());
__reflect(PomeloDemo.prototype, "PomeloDemo");
//# sourceMappingURL=PomeloDemo.js.map