"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Logger_1 = require("../../../util/Logger");
function default_1(app) {
    return new Handler(app);
}
exports.default = default_1;
class Handler {
    constructor(app) {
        this.app = app;
    }
    /**
     * 当有新的客户端连接上来时，把客户端 session 绑定以及将客户端加入到 channel 中
     *
     * @param  {Object}   msg     request message
     * @param  {Object}   session current session object
     */
    async entry(msg, session) {
        //处理uid和rid
        let self = this;
        let rid = msg.rid;
        let uid = msg.username + "*" + rid;
        if (!rid || !uid) {
            return {
                code: 500,
                msg: "用户名字和房间号码不能为空"
            };
        }
        //检测是否重复登录
        let sessionService = self.app.get("sessionService");
        if (!!sessionService.getByUid(uid)) {
            return {
                code: 500,
                msg: "不能重复登录"
            };
        }
        //绑定session
        await session.abind(uid);
        session.set("rid", rid);
        session.push("rid", function (err) {
            if (err) {
                Logger_1.logger.error("为session设置rid失败", err.stack);
            }
        });
        //当客户端断开连接时
        session.on("close", this.onUserLeave.bind(this));
        //把当前客户端加入channel
        let users = await self.app.rpc.chat.chatRemoter.add.route(session)(uid, self.app.getServerId(), rid, true);
        console.log("所有用户名字：", users);
        return { code: 200, msg: 'game server is ok.', data: {
                users: users
            } };
    }
    /** 当用户断开连接时 */
    onUserLeave(session) {
        if (!session || !session.uid) {
            return;
        }
        //跟后端服务器讲，将客户端从 channel 中移除（涉及rpc调用）
        this.app.rpc.chat.chatRemoter.kick.route(session)(session.uid, this.app.get("serverId"), session.get("rid"));
    }
}
exports.Handler = Handler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50cnlIYW5kbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vYXBwL3NlcnZlcnMvY29ubmVjdG9yL2hhbmRsZXIvZW50cnlIYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsaURBQThDO0FBRTlDLG1CQUF5QixHQUFnQjtJQUNyQyxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQzVCLENBQUM7QUFGRCw0QkFFQztBQUVELE1BQWEsT0FBTztJQUNoQixZQUFvQixHQUFnQjtRQUFoQixRQUFHLEdBQUgsR0FBRyxDQUFhO0lBRXBDLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBc0MsRUFBRSxPQUF3QjtRQUV4RSxXQUFXO1FBQ1gsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUM7UUFDbEIsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ25DLElBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUM7WUFDWixPQUFPO2dCQUNILElBQUksRUFBQyxHQUFHO2dCQUNSLEdBQUcsRUFBQyxlQUFlO2FBQ3RCLENBQUE7U0FDSjtRQUVELFVBQVU7UUFDVixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDaEMsT0FBTztnQkFDSCxJQUFJLEVBQUUsR0FBRztnQkFDVCxHQUFHLEVBQUUsUUFBUTthQUNoQixDQUFBO1NBQ0o7UUFFRCxXQUFXO1FBQ1gsTUFBTSxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLFVBQVUsR0FBRztZQUM3QixJQUFJLEdBQUcsRUFBRTtnQkFDTCxlQUFNLENBQUMsS0FBSyxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTthQUM3QztRQUNMLENBQUMsQ0FBQyxDQUFBO1FBRUYsV0FBVztRQUNYLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFFakQsaUJBQWlCO1FBQ2pCLElBQUksS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzRyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBQyxLQUFLLENBQUMsQ0FBQztRQUU3QixPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsb0JBQW9CLEVBQUMsSUFBSSxFQUFDO2dCQUMvQyxLQUFLLEVBQUMsS0FBSzthQUNkLEVBQUUsQ0FBQztJQUNSLENBQUM7SUFFRCxlQUFlO0lBQ2YsV0FBVyxDQUFDLE9BQXdCO1FBQ2hDLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFO1lBQzFCLE9BQU87U0FDVjtRQUVELG9DQUFvQztRQUNwQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDL0csQ0FBQztDQUVKO0FBaEVELDBCQWdFQyJ9