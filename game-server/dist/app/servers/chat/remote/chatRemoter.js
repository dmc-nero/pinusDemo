"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    return new ChatRemoter(app);
}
exports.default = default_1;
class ChatRemoter {
    constructor(app) {
        this.app = app;
        this.app = app;
        this.channelService = app.get("channelService");
    }
    /** 把客户端添加到channel
     * @param uid 用户的唯一id
     * @param sid 服务器id
     * @param name channel的名字
     * @param flag 如果该channel不存在，是否创建channel
     */
    async add(uid, sid, name, flag) {
        //创建一个channel
        let channel = this.channelService.getChannel(name, flag);
        //将用户加入channel
        if (!!channel) {
            channel.add(uid, sid);
        }
        console.log("当前sid：", sid);
        console.log("当前channel里的成员: ", channel.getMembers());
        //把当前加入的客户端，广播给所有客户端
        let userName = uid.split("*")[0];
        let param = {
            user: userName
        };
        channel.pushMessage("onAdd", param);
        // try {
        //     channel.pushMessage("onAdd", param);
        // } catch (error) {
        //     console.log("错误信息：",error);
        // }
        //返回当前channel里所有的用户名字
        return this.getUsersName(name, flag);
    }
    /** 获取某个channel里的所有用户名字
     * @param name channel的名字
     * @param flag 如果该channel不存在，是否创建channel
    */
    getUsersName(name, flag) {
        let users = [];
        //获取指定的channel
        let channel = this.channelService.getChannel(name, flag);
        //获取指定channel里加入的所有客户端
        if (!!channel) {
            users = channel.getMembers();
        }
        //返回所有客户端的名字
        for (let i = 0, len = users.length; i < len; i++) {
            users[i] = users[i].split("*")[0];
        }
        return users;
    }
    /** 将客户端踢出channel
     * @param uid 用户的唯一id
     * @param sid 服务器id
     * @param name channel的名字
    */
    async kick(uid, sid, name) {
        //获取指定的channel
        let channel = this.channelService.getChannel(name, false);
        //将指定客户端踢出channel
        if (!!channel) {
            channel.leave(uid, sid);
        }
        //通知当前channel里的所有客户端，说：某某离开了
    }
}
exports.ChatRemoter = ChatRemoter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhdFJlbW90ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9hcHAvc2VydmVycy9jaGF0L3JlbW90ZS9jaGF0UmVtb3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLG1CQUF5QixHQUFnQjtJQUNyQyxPQUFPLElBQUksV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2hDLENBQUM7QUFGRCw0QkFFQztBQW9CRCxNQUFhLFdBQVc7SUFDcEIsWUFBb0IsR0FBZ0I7UUFBaEIsUUFBRyxHQUFILEdBQUcsQ0FBYTtRQUNoQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFJRDs7Ozs7T0FLRztJQUNJLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxJQUFZLEVBQUUsSUFBYTtRQUVsRSxhQUFhO1FBQ2IsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRXpELGNBQWM7UUFDZCxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUU7WUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN6QjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7UUFFckQsb0JBQW9CO1FBQ3BCLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakMsSUFBSSxLQUFLLEdBQUc7WUFDUixJQUFJLEVBQUUsUUFBUTtTQUNqQixDQUFBO1FBRUQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFcEMsUUFBUTtRQUNSLDJDQUEyQztRQUMzQyxvQkFBb0I7UUFDcEIsa0NBQWtDO1FBQ2xDLElBQUk7UUFHSixxQkFBcUI7UUFDckIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7OztNQUdFO0lBQ00sWUFBWSxDQUFDLElBQVksRUFBRSxJQUFhO1FBRTVDLElBQUksS0FBSyxHQUFhLEVBQUUsQ0FBQztRQUV6QixjQUFjO1FBQ2QsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRXpELHNCQUFzQjtRQUN0QixJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUU7WUFDWCxLQUFLLEdBQUcsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ2hDO1FBRUQsWUFBWTtRQUNaLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDOUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDckM7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQ7Ozs7TUFJRTtJQUNLLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxJQUFZO1FBQ3BELGNBQWM7UUFDZCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFMUQsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRTtZQUNYLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO1FBRUQsNEJBQTRCO0lBRWhDLENBQUM7Q0FDSjtBQXJGRCxrQ0FxRkMifQ==