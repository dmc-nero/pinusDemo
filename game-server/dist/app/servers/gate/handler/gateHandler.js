"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Logger_1 = require("../../../util/Logger");
const dispatcher_1 = require("../../../util/dispatcher");
// var rpc_logger = require('pomelo-logger').getLogger('rpc-log', __filename);
function default_1(app) {
    return new Handler(app);
}
exports.default = default_1;
class Handler {
    constructor(app) {
        this.app = app;
    }
    /** 当客户端连接时，返回一个可用的connector服务器地址
     * @param  {Object}   msg     客户端请求信息，需要包含uid字段
     * @param  {Object}   session 当前客户端session对象
     */
    async entry(msg, session) {
        Logger_1.logger.info("当前服务器ID：", this.app.get("serverId"));
        let uid = msg.uid;
        if (!uid) {
            return {
                code: 500,
                msg: "用户uid字段不能为空"
            };
        }
        //获取所有 connector服务器
        let connectors = this.app.getServersByType("connector");
        if (!connectors || connectors.length === 0) {
            return {
                code: 500,
                msg: "当前没有可用的connector服务器"
            };
        }
        //通过算法，获取到 空闲的 connector服务器
        let con = dispatcher_1.dispatch(uid, connectors);
        return {
            code: 200,
            host: con.host,
            port: con.clientPort
        };
    }
}
exports.Handler = Handler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2F0ZUhhbmRsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9hcHAvc2VydmVycy9nYXRlL2hhbmRsZXIvZ2F0ZUhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxpREFBOEM7QUFDOUMseURBQW9EO0FBQ3BELDhFQUE4RTtBQUU5RSxtQkFBeUIsR0FBZ0I7SUFDckMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUM1QixDQUFDO0FBRkQsNEJBRUM7QUFFRCxNQUFhLE9BQU87SUFDaEIsWUFBb0IsR0FBZ0I7UUFBaEIsUUFBRyxHQUFILEdBQUcsQ0FBYTtJQUVwQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFvQixFQUFFLE9BQXdCO1FBQ3RELGVBQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDakQsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQztRQUNsQixJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ04sT0FBTztnQkFDSCxJQUFJLEVBQUUsR0FBRztnQkFDVCxHQUFHLEVBQUUsYUFBYTthQUNyQixDQUFBO1NBQ0o7UUFFRCxtQkFBbUI7UUFDbkIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsVUFBVSxJQUFJLFVBQVUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3hDLE9BQU87Z0JBQ0gsSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsR0FBRyxFQUFFLHFCQUFxQjthQUM3QixDQUFBO1NBQ0o7UUFFRCwyQkFBMkI7UUFDM0IsSUFBSSxHQUFHLEdBQUcscUJBQVEsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDcEMsT0FBTztZQUNILElBQUksRUFBRSxHQUFHO1lBQ1QsSUFBSSxFQUFDLEdBQUcsQ0FBQyxJQUFJO1lBQ2IsSUFBSSxFQUFDLEdBQUcsQ0FBQyxVQUFVO1NBQ3RCLENBQUM7SUFDTixDQUFDO0NBRUo7QUFyQ0QsMEJBcUNDIn0=