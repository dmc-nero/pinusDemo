import { getLogger } from 'pinus-logger';

let logger = getLogger("console");

export {
    logger,
    getLogger
};